import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    children: [{
      path: '',
      component: () => import('../components/WebTalkIntro')
    },
    {
      path: 'login/intro',
      component: () => import('../components/LoginIntro')
    },
    {
      path: 'register/intro',
      component: () => import('../components/RegisterIntro')
    },
    {
      path: 'area/intro',
      component: () => import('../components/AreaIntro')
    },
    {
      path: 'Calendar/intro',
      component: () => import('../components/CalendarIntro')
    }]
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../components/Login.vue')
  },
  {
    path: '/calendar',
    name: 'Calendar',
    component: () => import('../components/Calendar.vue')
  },
  {
    path: '/area',
    name: 'Area',
    component: () => import('../components/Area.vue')
  },
  {
    path: '/mix',
    name: 'Mix',
    component: () => import('../views/Mix.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
